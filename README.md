# food-truck-api


## Getting started
> install python
> install docker
> run pip install -r requirements.txt
> docker-compose up -d
> cd src/db
> alembic upgrade head
> export DATABASE_URL='postgresql://postgres:megasecret@localhost:5432/postgres'
> cd ../
> uvicorn src.main:app --reload
