from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os

engine = create_engine(os.environ.get('DATABASE_URL'))
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def to_date(str_date, format="%d-%m-%Y"):
    try:
        str_date = str(str_date)
        res = datetime.strptime(str_date, format)
        return res
    except ValueError as e:
        if 'does not match format' in str(e):
            res = datetime.strptime(str_date, "%Y-%d-%m")
            return res