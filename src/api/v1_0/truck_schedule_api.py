from fastapi import APIRouter, Depends
from sqlalchemy import Date
from src.db.dao.food_truck_schedule_dao import FoodTruckScheduleDao
from sqlalchemy.orm import Session
from src.utils import to_date
from src.db.models.food_truck_schedule import FoodTruckSchedule
from src.schema.food_truck_schedule_schema import FoodTruckScheduleSchema
from fastapi_sqlalchemy import db 
truck_schedule_router = APIRouter()

@truck_schedule_router.get("/food-truck-schedule", summary = "Retrieve food truck schedule", description="")
async def get_schedules(date: str = None):
    try:
        print(date)
        py_date = None
        if(date):
            py_date = to_date(str(date), "%d-%m-%Y %H:%M:%S")
        response = FoodTruckScheduleDao.get_schedules(db.session, py_date ) 
        print(response)
        if(not response['status']):
            return response
        return  {"status": True, "data": response['data']}
    except Exception as e:
        return  {"status":  False, "message": "Failed to get food truck schedule {}".format(e)}
    


@truck_schedule_router.post("/food-truck-schedule", summary = "Save food truck schedule", description="")
async def save_schedule( food_truck_schedule_schema: FoodTruckScheduleSchema):
    try:
        print(food_truck_schedule_schema)
        py_date = to_date(str(food_truck_schedule_schema.date), "%d-%m-%Y %H:%M:%S")
        response = FoodTruckScheduleDao.save_food_truck_schedule(db.session, id=food_truck_schedule_schema.id,
                                                      name=food_truck_schedule_schema.name,
                                                      date = py_date)
        if(not response['status']):
            return response
        print(response)
        return  {"status":True, "message":"Saved Successfully"}
    except Exception as e:
        print(e)
        return   {"status":False, "message":"Failed to save {}".format(e)}