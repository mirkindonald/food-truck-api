from src.db.models.food_truck_schedule import FoodTruckSchedule
from flask_restful import marshal, fields


class FoodTruckScheduleDao():

    @classmethod
    def save_food_truck_schedule(cls, db_session, **kwargs):
        try:
            schedule = FoodTruckSchedule(**kwargs)
            if(schedule.id==0):
                schedule.id = None
                db_session.add(schedule)
            else:
                db_session.merge(schedule)
            db_session.commit()
            return {"status": True, "message": "Saved successfully"}

        except Exception as ex:
            return {"status": False, "message": "Failed to save food schedule details {}".format(str(ex))}

    @classmethod
    def get_schedules(cls, db_session, date=None):
        try:
            query = db_session.query(FoodTruckSchedule)
            if date:
                query = query.filter_by(date=date)
            schedules = query.all()
            print(schedules)
            if not schedules:
                return {"status": False, "message": "No records found for Food schedule"}
            schedules_dict = marshal(schedules, ScheduleResource.fields)
            print(schedules_dict)
            return {"status": True, "data": schedules_dict}
        except Exception as e:
            return {"status": False, "message": "Error occurred while retrieving Food truck schedules: {}".format(str(e))}


class ScheduleResource:
    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'date': fields.DateTime
    }
