
from src.db.config import Base
from sqlalchemy import Column, DateTime, Integer, String


class FoodTruckSchedule(Base):
    """
    FoodTruckSchedule Model
    """
    __tablename__ = 'food_truck_schedule'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String(length=500))
    date = Column('date', DateTime, nullable=False)