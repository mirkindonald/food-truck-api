"""Initial tables

Revision ID: 4c275d2ce5a6
Revises: 
Create Date: 2023-03-10 17:58:48.405359

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4c275d2ce5a6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('food_truck_schedule',
                    sa.Column('id', sa.Integer, nullable=False),
                    sa.Column('name', sa.String(length=100), nullable=False),
                    sa.Column('date', sa.DateTime),
                    sa.PrimaryKeyConstraint('id'))


def downgrade():
     op.drop_table('food_truck_schedule')
