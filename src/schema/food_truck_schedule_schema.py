import datetime
from typing import Union
from pydantic import BaseModel


class FoodTruckScheduleSchema(BaseModel):
    id: int
    name: str
    date: str
