from http.client import BAD_REQUEST
import os
from fastapi.middleware.cors import CORSMiddleware
from fastapi_sqlalchemy import DBSessionMiddleware  # middleware helper

from fastapi import FastAPI
from pydantic import BaseSettings
from src.api.v1_0.truck_schedule_api import truck_schedule_router

app = FastAPI()
app.add_middleware(DBSessionMiddleware, db_url=os.getenv("DATABASE_URL"))

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:4400",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(truck_schedule_router, prefix="/v1.0")